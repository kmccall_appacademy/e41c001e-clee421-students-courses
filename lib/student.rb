class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name, courses=[])
    @first_name = first_name
    @last_name = last_name
    @courses = courses
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    unless @courses.include?(new_course) || course_conflict?(new_course)
      new_course.students << self
      @courses << new_course
    end
  end #enroll

  def course_load
    new_hash = Hash.new(0)
    courses.each {|c| new_hash[c.department] += c.credits}
    new_hash
  end #course_load

  def course_conflict?(course)
    @courses.each {|c| raise "Course conflict!" if course.conflicts_with?(c)}
    false
  end
end #class Student
